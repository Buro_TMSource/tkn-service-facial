package com.teknei.bid.service;

import com.teknei.bid.dto.DocumentPictureRequestDTO;
import com.teknei.bid.persistence.repository.BidCurpRepository;
import com.teknei.bid.util.tas.TasManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FacialService {

    @Autowired
    private BidCurpRepository bidCurpRepository;
    @Autowired
    private TasManager tasManager;

    public byte[] findPicture(DocumentPictureRequestDTO requestDTO) {
        return tasManager.getPicture(requestDTO.getId());
    }

}