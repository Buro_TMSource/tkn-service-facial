package com.teknei.bid.service.remote;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "BID-SERV-BIOMETRIC")
public interface FacialVerificationService {

    @RequestMapping(value = "/biometric/search/customerFace", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    ResponseEntity<String> searchByFace(@RequestBody String jsonStringRequest);

}