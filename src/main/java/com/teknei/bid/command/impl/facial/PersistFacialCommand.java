package com.teknei.bid.command.impl.facial;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.persistence.entities.*;
import com.teknei.bid.persistence.repository.BidCurpRepository;
import com.teknei.bid.persistence.repository.BidFacialRepository;
import com.teknei.bid.persistence.repository.BidRegProcRepository;
import com.teknei.bid.persistence.repository.BidSelfRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public class PersistFacialCommand implements Command {

    @Autowired
    private BidRegProcRepository bidRegProcRepository;
    @Autowired
    private BidCurpRepository bidCurpRepository;
    @Autowired
    private BidFacialRepository bidFacialRepository;
    @Autowired
    private BidSelfRepository bidSelfRepository;

    private static final Logger log = LoggerFactory.getLogger(PersistFacialCommand.class);

    @Override
    public CommandResponse execute(CommandRequest request) {
    	//log.info("lblancas:: [tkn-service-facial] :: "+this.getClass().getName()+".execute ");
        CommandResponse commandResponse = new CommandResponse();
        commandResponse.setDocumentId(request.getDocumentId());
        commandResponse.setScanId(request.getScanId());
        commandResponse.setId(request.getId());
        try {
            alterRecord(request.getId(), true, request.getUsername());
            registerFacial(request.getId(), request.getUsername());
            registerSelf(request.getId(), request.getUsername());
            commandResponse.setStatus(Status.FACIAL_DB_OK);
        } catch (Exception e) {
            log.error("Error in PersistFacialCommand: {}", e.getMessage());
            commandResponse.setStatus(Status.FACIAL_DB_ERROR);
        }
        return commandResponse;
    }

    private void registerSelf(Long idCustomer, String username) {
    	//log.info("lblancas:: [tkn-service-facial] :: "+this.getClass().getName()+".registerSelf ");
        BidSelf self = new BidSelf();
        self.setFchCrea(new Timestamp(System.currentTimeMillis()));
        self.setIdClie(idCustomer);
        self.setIdEsta(1);
        self.setIdSelf(idCustomer);
        self.setIdTipo(3);
        self.setPorcComp((short) 100);
        self.setUsrCrea(username);
        self.setUsrOpeCrea(username);
        bidSelfRepository.save(self);
    }

    private void registerFacial(Long idClie, String username) {
    	//log.info("lblancas:: [tkn-service-facial] :: "+this.getClass().getName()+".registerFacial ");
        BidClieFaci faci = new BidClieFaci();
        faci.setIdFaci(idClie);
        faci.setIdClie(idClie);
        faci.setUsrCrea(username);
        faci.setUsrOpeCrea(username);
        faci.setIdEsta(1);
        faci.setIdTipo(3);
        faci.setFchCrea(new Timestamp(System.currentTimeMillis()));
        bidFacialRepository.save(faci);
    }

    private void alterRecord(Long operationId, boolean created, String username) 
    {
    	//log.info("lblancas:: [tkn-service-facial] :: "+this.getClass().getName()+".alterRecord ");
        BidClieCurp curp = bidCurpRepository.findTopByIdClie(operationId);
        BidClieRegProcPK pk = new BidClieRegProcPK();
        pk.setIdClie(operationId);
        pk.setCurp(curp.getCurp());
        BidClieRegProc proc = bidRegProcRepository.findOne(pk);
        proc.setRegFaci(created);
        proc.setFchRegFaci(new Timestamp(System.currentTimeMillis()));
        proc.setUsrOpeCrea(username);
        proc.setUsrCrea(username);
        proc.setIdEsta(1);
        proc.setIdTipo(3);
        proc.setFchCrea(new Timestamp(System.currentTimeMillis()));
        bidRegProcRepository.save(proc);
    }
}
