package com.teknei.bid.command.impl.facial;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.service.remote.FacialVerificationService;
import feign.FeignException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;
import org.springframework.web.multipart.MultipartFile;

@Component
public class ParseFacialCommand implements Command {

    private static final Logger log = LoggerFactory.getLogger(ParseFacialCommand.class);
    @Autowired
    private FacialVerificationService facialVerificationService;

    @Override
    public CommandResponse execute(CommandRequest request) 
    {
    	//log.info("lblancas:: [tkn-service-facial] :: "+this.getClass().getName()+".execute id: "+String.valueOf(request.getId()));
        CommandResponse response = new CommandResponse();
        try {
            byte[] content = request.getFileContent().get(0);
            String contentB64 = Base64Utils.encodeToString(content);
            JSONObject jsonRequest = new JSONObject();
            jsonRequest.put("id", String.valueOf(request.getId()));
            jsonRequest.put("facial", contentB64);
            //log.info("lblancas:: [tkn-service-facial] :: "+this.getClass().getName()+".execute "+ "{\"id\":\""+    String.valueOf(request.getId())+"\"," +  "\"facial\":\""+(contentB64==null?"NULL":"<contentB64>")+"\"}");
            log.info("Sending request to BIOM: {}", System.currentTimeMillis());
            ResponseEntity<String> responseEntity = facialVerificationService.searchByFace(jsonRequest.toString());
            JSONObject jsonObject = new JSONObject(responseEntity.getBody());
            String id = jsonObject.getString("id");
            response.setStatus(Status.FACIAL_MBSS_ERROR);
            response.setDesc(id);
        } catch (FeignException e) {
            switch (e.status()){
                case 404:
                    response.setStatus(Status.FACIAL_MBSS_OK);
                    response.setDesc("OK");
                    break;
                case 500:
                    response.setStatus(Status.FACIAL_MBSS_ERROR);
                    response.setDesc("Error request");
                    log.error("Error parsing facial to biom with message: {}", e.getMessage());
                    e.printStackTrace();
                    break;
                default:
                    response.setStatus(Status.FACIAL_MBSS_ERROR);
                    response.setDesc("Error request");
                    log.error("Error parsing facial to biom with message: {}", e.getMessage());
                    e.printStackTrace();
                    break;
            }
        }catch(Exception e){
            response.setStatus(Status.FACIAL_MBSS_ERROR);
            response.setDesc("Error request");
            log.error("Error parsing facial to biom with message: {}", e.getMessage());
            log.error("Time:{}", System.currentTimeMillis());
            e.printStackTrace();
        }
        //log.info("lblancas:: [tkn-service-facial] :: "+this.getClass().getName()+".execute Estatus: "+response.getStatus());
        //log.info("lblancas:: [tkn-service-facial] :: "+this.getClass().getName()+".execute Descrip: "+response.getDesc());
        
        return response;

    }

}
